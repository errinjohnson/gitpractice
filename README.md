# README #
### What is this repository for? ### 
* GOAL: The goal of this exercise is to introduce the idea of encountering a conflict committing files and how to merge our changes once you have encounters such a situation. This is a common issue in scenarios where more than one developer will be working in the same solution at the same time.  
 
* Task 1:
        Ward is ready to begin development and is going to create a new repository on git hub. 
        We will create a new repository to keep this separate from the rest of the pre-work exercises.
* Task 2:
        With the repository created, Ward is ready to create the first file and check in the initial file to the 
        and push those changes to Bitbucket; With the repository created, Ward is ready to create the first file and
        check in the initial file to the repository and push those changes to Bitbucket. 
* Task 3: 
        Now Wise is now going to join the development effort. We will simulate this by creating 
        a separate directory for his repository and modify the file already in the directory.
        Once modified we will jump to Ward’s file and modify that. Let’s see what happens when he tries to push the file. 
* Task 4:
        Remember when Wise made modifications, committed them and then pushed them up to Bitbucket. 
        Well, Ward didn't    pull those changes and now the file is out of sync with the remote repository and cannot be 
        until Ward pulls these changes and merges them with the changes he wishes to make. To do this we need to pull 
        those changes that are in the remote repository on Bitbucket. To get those changes we will start by pulling them 
        down, merging changes and then recommitting and pushing to Bitbucket. 

### How do I get set up? ###

* Summary of set up
* Configuration:  global config:
   user.name = username , user.email = user@me.com
* Dependencies:  git
* Database configuration: none
* How to run tests: local machine - preferred editor
* Deployment instructions: git bash

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact